using PeluditoHexagonal;
using PeluditoHexagonal.Shared.Infrastructure.Context;
using PeluditoHexagonal.Shared.Infrastructure.DapperContext;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers().AddJsonOptions(options =>
{
    options.JsonSerializerOptions.DefaultIgnoreCondition = System.Text.Json.Serialization.JsonIgnoreCondition.WhenWritingNull;
});

builder.Services.Configure<ApiBehaviorOptions>(options =>
{
    options.SuppressModelStateInvalidFilter = true;
});

//Jwt configuration starts here
var jwtKey = builder.Configuration.GetSection("Jwt:Key").Get<string>() ?? string.Empty;

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
.AddJwtBearer(options =>
{
    options.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateIssuer = false,
        ValidateAudience = false,
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtKey))
    };
});
//Jwt configuration ends here

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


// Dapper context
builder.Services.AddSingleton<DapperContext>();

builder.Services.AddDbContext<AppDbContext>(options => options.UseLazyLoadingProxies(true).UseSqlServer(builder.Configuration.GetConnectionString("DatabaseConnection")));

builder.Services.AddSeederExtension();

builder.Services.AddTagExtension();
builder.Services.AddUserExtension();
builder.Services.AddCategoryExtension();
builder.Services.AddNoteExtension();
builder.Services.AddLoginExtension();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
