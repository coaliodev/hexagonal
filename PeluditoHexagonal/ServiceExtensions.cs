﻿using PeluditoHexagonal.Features.Category.Application.Services;
using PeluditoHexagonal.Features.Category.Application.UseCases;
using PeluditoHexagonal.Features.Category.Domain.Ports.In;
using PeluditoHexagonal.Features.Category.Domain.Ports.Out;
using PeluditoHexagonal.Features.Category.Infrastructure;
using PeluditoHexagonal.Features.Note.Application.Services;
using PeluditoHexagonal.Features.Note.Application.UseCases;
using PeluditoHexagonal.Features.Note.Domain.Ports.In;
using PeluditoHexagonal.Features.Note.Domain.Ports.Out;
using PeluditoHexagonal.Features.Note.Infrastructure;
using PeluditoHexagonal.Features.Login.Application.UseCases;
using PeluditoHexagonal.Features.Login.Domain.Ports.In;
using PeluditoHexagonal.Features.Tag.Application.Services;
using PeluditoHexagonal.Features.Tag.Application.UseCases;
using PeluditoHexagonal.Features.Tag.Domain.Ports.In;
using PeluditoHexagonal.Features.Tag.Domain.Ports.Out;
using PeluditoHexagonal.Features.Tag.Infrastructure;
using PeluditoHexagonal.Features.User.Application.Services;
using PeluditoHexagonal.Features.User.Application.UseCases;
using PeluditoHexagonal.Features.User.Domain.Ports.In;
using PeluditoHexagonal.Features.User.Domain.Ports.Out;
using PeluditoHexagonal.Features.User.Infrastructure;
using PeluditoHexagonal.Shared.Infrastructure.Context;
using Microsoft.EntityFrameworkCore;

namespace PeluditoHexagonal
{
    public static class ServiceExtensions
    {
        public static void AddSeederExtension(this IServiceCollection services)
        {
            ServiceProvider serviceProvider = services.BuildServiceProvider();
            using (AppDbContext? context = serviceProvider.GetService<AppDbContext>())
            {
                try {
                    if (context != null) {
                        context.Database.Migrate();
                        SeedData.Initialize(context);
                    }
                } catch (Exception ex) {
                    Console.WriteLine($"Couldnt start db because '{ex.Message}'");
                }
            }
        }

        public static void AddCategoryExtension(this IServiceCollection services)
        {
            services.AddScoped<ICategoryDbAdapterPort, CategoryDbAdapter>();
            services.AddScoped<ICategoryServicePort, CategoryService>();
            services.AddScoped<IGetCategories, GetCategoriesImpl>();
            services.AddScoped<IGetCategoryById, GetCategoryByIdImpl>();
            services.AddScoped<ICreateCategory, CreateCategoryImpl>();
            services.AddScoped<IUpdateCategory, UpdateCategoryImpl>();
            services.AddScoped<IDeleteCategory, DeleteCategoryImpl>();
        }
        public static void AddNoteExtension(this IServiceCollection services)
        {
            services.AddScoped<INoteDbAdapterPort, NoteDbAdapter>();
            services.AddScoped<INoteServicePort, NoteService>();
            services.AddScoped<IGetNotes, GetNotesImpl>();
            services.AddScoped<IGetNoteById, GetNoteByIdImpl>();
            services.AddScoped<ICreateNote, CreateNoteImpl>();
            services.AddScoped<IUpdateNote, UpdateNoteImpl>();
            services.AddScoped<IDeleteNote, DeleteNoteImpl>();
            services.AddScoped<IAddTags, AddTagsImpl>();
            services.AddScoped<IRemoveTags, RemoveTagsImpl>();
        }

        public static void AddUserExtension(this IServiceCollection services)
        {
            services.AddScoped<IUserDbAdapterPort, UserDbAdapter>();
            services.AddScoped<IGetUsers, GetUsersImpl>();
            services.AddScoped<IGetUserById, GetUserByIdImpl>();
            services.AddScoped<IGetUserByEmail, GetUserByEmailImpl>();
            services.AddScoped<ICreateUser, CreateUserImpl>();
            services.AddScoped<IUpdateUser, UpdateUserImpl>();
            services.AddScoped<IDeleteUser, DeleteUserImpl>();
            services.AddScoped<IUserServicePort, UserService>();
        }

        public static void AddTagExtension(this IServiceCollection services)
        {
            services.AddScoped<ITagDbAdapterPort, TagDbAdapter>();
            services.AddScoped<IGetTags, GetTagsImpl>();
            services.AddScoped<IGetTagById, GetTagByIdImpl>();
            services.AddScoped<ICreateTag, CreateTagImpl>();
            services.AddScoped<IUpdateTag, UpdateTagImpl>();
            services.AddScoped<IDeleteTag, DeleteTagImpl>();
            services.AddScoped<ITagServicePort, TagService>();
        }

        public static void AddLoginExtension(this IServiceCollection services)
        {
            services.AddScoped<ILogin, LoginImpl>();
        }
    }
}
