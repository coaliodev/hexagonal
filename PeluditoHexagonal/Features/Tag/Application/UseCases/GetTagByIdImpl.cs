﻿using PeluditoHexagonal.Features.Tag.Domain.Models;
using PeluditoHexagonal.Features.Tag.Domain.Ports.In;
using PeluditoHexagonal.Features.Tag.Domain.Ports.Out;

namespace PeluditoHexagonal.Features.Tag.Application.UseCases
{
    public class GetTagByIdImpl : IGetTagById
    {
        private readonly ITagDbAdapterPort _adapter;

        public GetTagByIdImpl(ITagDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<TagModel> GetTagById(int id)
        {
            return _adapter.FindById(id);
        }
    }
}
