﻿using PeluditoHexagonal.Features.Tag.Domain.Models;
using PeluditoHexagonal.Features.Tag.Domain.Ports.In;
using PeluditoHexagonal.Features.Tag.Domain.Ports.Out;

namespace PeluditoHexagonal.Features.Tag.Application.UseCases
{
    public class GetTagsImpl : IGetTags
    {
        private readonly ITagDbAdapterPort _adapter;

        public GetTagsImpl(ITagDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<List<TagModel>> GetTags()
        {
            return _adapter.FindAll();
        }
    }
}
