﻿using PeluditoHexagonal.Features.Tag.Domain.Models;
using PeluditoHexagonal.Features.Tag.Domain.Ports.In;
using PeluditoHexagonal.Features.Tag.Domain.Ports.Out;

namespace PeluditoHexagonal.Features.Tag.Application.UseCases
{
    public class CreateTagImpl : ICreateTag
    {
        private readonly ITagDbAdapterPort _adapter;

        public CreateTagImpl(ITagDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<TagModel> Create(TagModel tag)
        {
            return _adapter.Create(tag);
        }
    }
}
