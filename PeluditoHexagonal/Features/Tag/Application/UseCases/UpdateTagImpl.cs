﻿using PeluditoHexagonal.Features.Tag.Domain.Models;
using PeluditoHexagonal.Features.Tag.Domain.Ports.In;
using PeluditoHexagonal.Features.Tag.Domain.Ports.Out;

namespace PeluditoHexagonal.Features.Tag.Application.UseCases
{
    public class UpdateTagImpl : IUpdateTag
    {
        private readonly ITagDbAdapterPort _adapter;

        public UpdateTagImpl(ITagDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<TagModel> Update(int id, TagModel tag)
        {
            return _adapter.Update(id, tag);
        }
    }
}
