﻿using PeluditoHexagonal.Features.Tag.Domain.Models;
using PeluditoHexagonal.Features.Tag.Domain.Ports.In;

namespace PeluditoHexagonal.Features.Tag.Application.Services
{
    public class TagService : ITagServicePort
    {
        private readonly IGetTags _getTags;
        private readonly IGetTagById _getTagById;
        private readonly ICreateTag _createTag;
        private readonly IUpdateTag _updateTag;
        private readonly IDeleteTag _deleteTag;

        public TagService(IGetTags getTags, IGetTagById getTagById, ICreateTag createTag, IUpdateTag updateTag, IDeleteTag deleteTag)
        {
            _getTags = getTags;
            _getTagById = getTagById;
            _createTag = createTag;
            _updateTag = updateTag;
            _deleteTag = deleteTag;
        }

        public Task<List<TagModel>> GetTags()
        {
            return _getTags.GetTags();
        }

        public Task<TagModel> GetTagById(int id)
        {
            return _getTagById.GetTagById(id);
        }

        public Task<TagModel> Create(TagModel tag)
        {
            return _createTag.Create(tag);
        }

        public Task<TagModel> Update(int id, TagModel tag)
        {
            return _updateTag.Update(id, tag);
        }

        public Task<int> Delete(int id)
        {
            return _deleteTag.Delete(id);
        }
    }
}
