﻿using PeluditoHexagonal.Features.Tag.Domain.Models;
using PeluditoHexagonal.Features.Tag.Domain.Ports.In;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace PeluditoHexagonal.Features.Tag.Infrastructure
{
    [Route("api/Tags")]
    [ApiController]
    public class TagController : ControllerBase
    {
        private readonly ITagServicePort _service;

        public TagController(ITagServicePort service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TagModel>>> Get()
        {
            return await _service.GetTags();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TagModel>> Get(int id)
        {
            TagModel tagModel = await _service.GetTagById(id);
            if (tagModel == null)
            {
                return NotFound();
            }
            return tagModel;
        }

        [HttpPost]
        public async Task<ActionResult<TagModel>> Post([FromBody] TagModel tagModel)
        {
            try
            {
                TagModel modelSaved = await _service.Create(tagModel);
                return CreatedAtAction("Get", new { id = modelSaved.Id }, modelSaved);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPatch("{id}")]
        public async Task<IActionResult> Patch(int id, [FromBody] TagModel tagModel)
        {
            try
            {
                await _service.Update(id, tagModel);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _service.Delete(id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return NoContent();
        }
    }
}
