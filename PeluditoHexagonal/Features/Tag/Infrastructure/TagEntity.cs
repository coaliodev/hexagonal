﻿using PeluditoHexagonal.Features.Tag.Domain.Models;
using PeluditoHexagonal.Features.User.Infrastructure;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PeluditoHexagonal.Features.Tag.Infrastructure
{
    [Table("Tags")]
    public class TagEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(100)]
        public string? Description { get; set; }

        public virtual ICollection<UserEntity> Users { get; set; }

        public TagEntity FromModel(TagModel model)
        {
            Id = model.Id;
            Name = model.Name;
            Description = model.Description;
            return this;
        }

        public TagModel ToModel()
        {
            return new TagModel
            {
                Id = Id,
                Name = Name,
                Description = Description
            };
        }

        public void UpdatePropsFromModel(TagModel model)
        {
            Name = model.Name ?? Name;
            Description = model.Description ?? Description;
        }
    }
}
