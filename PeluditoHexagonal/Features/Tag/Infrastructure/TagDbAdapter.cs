﻿using PeluditoHexagonal.Features.Tag.Domain.Models;
using PeluditoHexagonal.Features.Tag.Domain.Ports.Out;
using PeluditoHexagonal.Shared.Infrastructure.Context;
using Microsoft.EntityFrameworkCore;

namespace PeluditoHexagonal.Features.Tag.Infrastructure
{
    public class TagDbAdapter : ITagDbAdapterPort
    {
        private readonly AppDbContext _db;

        public TagDbAdapter(AppDbContext context)
        {
            _db = context;
        }

        public async Task<List<TagModel>> FindAll()
        {
            List<TagModel> tags = await _db.Tags
                .Select(x => x.ToModel())
                .ToListAsync();

            return tags;
        }

        public async Task<TagModel> FindById(int id)
        {
            TagEntity? tagFound = await _db.Tags
                .FirstOrDefaultAsync(i => i.Id == id);

            return tagFound == null
                ? throw new Exception($"Tag with id {id} not found")
                : tagFound.ToModel();
        }

        public async Task<TagModel> Create(TagModel tag)
        {
            try
            {
                TagEntity entity = new TagEntity().FromModel(tag);
                _db.Tags.Add(entity);
                await _db.SaveChangesAsync();
                return entity.ToModel();
            }
            catch (Exception e)
            {
                throw new Exception("Error creating tag", e);
            }
        }

        public async Task<TagModel> Update(int id, TagModel tag)
        {
            TagEntity? entity = await _db.Tags.FirstOrDefaultAsync(x => x.Id == id);

            if (entity == null)
            {
                throw new Exception("Tag not found");
            }

            try
            {
                entity.UpdatePropsFromModel(tag);

                _db.Entry(entity).State = EntityState.Modified;
                await _db.SaveChangesAsync();

                return entity.ToModel();
            }
            catch (Exception e)
            {
                throw new Exception("Error updating tag", e);
            }
        }

        public async Task<int> Delete(int id)
        {
            TagEntity? tagEntity = await _db.Tags.FindAsync(id);

            if (tagEntity == null)
            {
                throw new Exception("Tag not found");
            }

            try
            {
                _db.Tags.Remove(tagEntity);

                await _db.SaveChangesAsync();

                return id;
            }
            catch (Exception e)
            {
                throw new Exception("Error deleting tag", e);
            }
        }
    }
}
