﻿using PeluditoHexagonal.Features.Note.Domain.Models;
using System.ComponentModel.DataAnnotations;

namespace PeluditoHexagonal.Features.Tag.Domain.Models
{
    public class TagModel
    {
        public int Id { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(100)]
        public string? Description { get; set; }

        public virtual ICollection<NoteModel> Notes { get; set; }
    }
}
