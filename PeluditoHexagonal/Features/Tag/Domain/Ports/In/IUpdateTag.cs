﻿using PeluditoHexagonal.Features.Tag.Domain.Models;

namespace PeluditoHexagonal.Features.Tag.Domain.Ports.In
{
    public interface IUpdateTag
    {
        Task<TagModel> Update(int id, TagModel tag);
    }
}
