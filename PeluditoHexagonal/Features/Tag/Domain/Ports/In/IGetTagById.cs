﻿using PeluditoHexagonal.Features.Tag.Domain.Models;

namespace PeluditoHexagonal.Features.Tag.Domain.Ports.In
{
    public interface IGetTagById
    {
        Task<TagModel> GetTagById(int id);
    }
}
