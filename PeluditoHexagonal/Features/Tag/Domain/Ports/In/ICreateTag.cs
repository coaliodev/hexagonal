﻿using PeluditoHexagonal.Features.Tag.Domain.Models;

namespace PeluditoHexagonal.Features.Tag.Domain.Ports.In
{
    public interface ICreateTag
    {
        Task<TagModel> Create(TagModel Tag);
    }
}
