﻿using PeluditoHexagonal.Features.Tag.Domain.Models;

namespace PeluditoHexagonal.Features.Tag.Domain.Ports.In
{
    public interface IGetTags
    {
        Task<List<TagModel>> GetTags();
    }
}
