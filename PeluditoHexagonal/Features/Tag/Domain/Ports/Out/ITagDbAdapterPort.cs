﻿using PeluditoHexagonal.Features.Tag.Domain.Models;
using PeluditoHexagonal.Shared.Domain.Ports.Out;

namespace PeluditoHexagonal.Features.Tag.Domain.Ports.Out
{
    public interface ITagDbAdapterPort : IDbCrudAdapter<TagModel>
    {
    }
}
