﻿using PeluditoHexagonal.Features.Note.Domain.Models;
using System.ComponentModel.DataAnnotations;

namespace PeluditoHexagonal.Features.Category.Domain.Models
{
    public class CategoryModel
    {
        public int Id { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(100)]
        public string? Description { get; set; }

        public virtual ICollection<NoteModel> Notes { get; set; }
    }
}
