﻿using PeluditoHexagonal.Features.Category.Domain.Models;

namespace PeluditoHexagonal.Features.Category.Domain.Ports.In
{
    public interface IUpdateCategory
    {
        Task<CategoryModel> Update(int id, CategoryModel Category);
    }
}
