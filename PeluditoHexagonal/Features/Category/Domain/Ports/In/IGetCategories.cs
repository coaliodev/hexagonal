﻿using PeluditoHexagonal.Features.Category.Domain.Models;

namespace PeluditoHexagonal.Features.Category.Domain.Ports.In
{
    public interface IGetCategories
    {
        Task<List<CategoryModel>> GetCategories();
    }
}
