﻿using PeluditoHexagonal.Features.Category.Domain.Models;

namespace PeluditoHexagonal.Features.Category.Domain.Ports.In
{
    public interface IGetCategoryById
    {
        Task<CategoryModel> GetCategoryById(int id);
    }
}
