﻿using PeluditoHexagonal.Features.Category.Domain.Models;

namespace PeluditoHexagonal.Features.Category.Domain.Ports.In
{
    public interface ICreateCategory
    {
        Task<CategoryModel> Create(CategoryModel Category);
    }
}
