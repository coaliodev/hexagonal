﻿using PeluditoHexagonal.Features.Category.Domain.Models;
using PeluditoHexagonal.Shared.Domain.Ports.Out;

namespace PeluditoHexagonal.Features.Category.Domain.Ports.Out
{
    public interface ICategoryDbAdapterPort : IDbCrudAdapter<CategoryModel>
    {
    }
}
