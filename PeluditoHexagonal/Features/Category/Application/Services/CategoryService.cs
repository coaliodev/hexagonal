﻿using PeluditoHexagonal.Features.Category.Domain.Models;
using PeluditoHexagonal.Features.Category.Domain.Ports.In;

namespace PeluditoHexagonal.Features.Category.Application.Services
{
    public class CategoryService : ICategoryServicePort
    {
        private readonly IGetCategories _getCategories;
        private readonly IGetCategoryById _getCategoryById;
        private readonly ICreateCategory _createCategory;
        private readonly IUpdateCategory _updateCategory;
        private readonly IDeleteCategory _deleteCategory;

        public CategoryService(IGetCategories getCategories, IGetCategoryById getCategoryById, ICreateCategory createCategory, IUpdateCategory updateCategory, IDeleteCategory deleteCategory)
        {
            _getCategories = getCategories;
            _getCategoryById = getCategoryById;
            _createCategory = createCategory;
            _updateCategory = updateCategory;
            _deleteCategory = deleteCategory;
        }

        public Task<List<CategoryModel>> GetCategories()
        {
            return _getCategories.GetCategories();
        }

        public Task<CategoryModel> GetCategoryById(int id)
        {
            return _getCategoryById.GetCategoryById(id);
        }

        public Task<CategoryModel> Create(CategoryModel Category)
        {
            return _createCategory.Create(Category);
        }

        public Task<CategoryModel> Update(int id, CategoryModel Category)
        {
            return _updateCategory.Update(id, Category);
        }

        public Task<int> Delete(int id)
        {
            return _deleteCategory.Delete(id);
        }
    }
}
