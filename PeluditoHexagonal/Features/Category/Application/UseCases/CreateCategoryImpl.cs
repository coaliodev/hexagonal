﻿using PeluditoHexagonal.Features.Category.Domain.Models;
using PeluditoHexagonal.Features.Category.Domain.Ports.In;
using PeluditoHexagonal.Features.Category.Domain.Ports.Out;

namespace PeluditoHexagonal.Features.Category.Application.UseCases
{
    public class CreateCategoryImpl : ICreateCategory
    {
        private readonly ICategoryDbAdapterPort _adapter;

        public CreateCategoryImpl(ICategoryDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<CategoryModel> Create(CategoryModel Category)
        {
            return _adapter.Create(Category);
        }
    }
}
