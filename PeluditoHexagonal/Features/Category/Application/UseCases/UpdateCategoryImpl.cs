﻿using PeluditoHexagonal.Features.Category.Domain.Models;
using PeluditoHexagonal.Features.Category.Domain.Ports.In;
using PeluditoHexagonal.Features.Category.Domain.Ports.Out;

namespace PeluditoHexagonal.Features.Category.Application.UseCases
{
    public class UpdateCategoryImpl : IUpdateCategory
    {
        private readonly ICategoryDbAdapterPort _adapter;

        public UpdateCategoryImpl(ICategoryDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<CategoryModel> Update(int id, CategoryModel Category)
        {
            return _adapter.Update(id, Category);
        }
    }
}
