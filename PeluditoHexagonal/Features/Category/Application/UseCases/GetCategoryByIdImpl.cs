﻿using PeluditoHexagonal.Features.Category.Domain.Models;
using PeluditoHexagonal.Features.Category.Domain.Ports.In;
using PeluditoHexagonal.Features.Category.Domain.Ports.Out;

namespace PeluditoHexagonal.Features.Category.Application.UseCases
{
    public class GetCategoryByIdImpl : IGetCategoryById
    {
        private readonly ICategoryDbAdapterPort _adapter;

        public GetCategoryByIdImpl(ICategoryDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<CategoryModel> GetCategoryById(int id)
        {
            return _adapter.FindById(id);
        }
    }
}
