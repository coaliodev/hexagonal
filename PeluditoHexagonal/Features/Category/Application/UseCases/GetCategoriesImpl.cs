﻿using PeluditoHexagonal.Features.Category.Domain.Models;
using PeluditoHexagonal.Features.Category.Domain.Ports.In;
using PeluditoHexagonal.Features.Category.Domain.Ports.Out;

namespace PeluditoHexagonal.Features.Category.Application.UseCases
{
    public class GetCategoriesImpl : IGetCategories
    {
        private readonly ICategoryDbAdapterPort _adapter;

        public GetCategoriesImpl(ICategoryDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<List<CategoryModel>> GetCategories()
        {
            return _adapter.FindAll();
        }
    }
}
