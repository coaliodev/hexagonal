﻿using PeluditoHexagonal.Features.Category.Domain.Models;
using PeluditoHexagonal.Features.Category.Domain.Ports.In;
using Microsoft.AspNetCore.Mvc;

namespace PeluditoHexagonal.Features.Category.Infrastructure
{
    [Route("api/Categories")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryServicePort _service;

        public CategoryController(ICategoryServicePort service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<CategoryModel>>> Get()
        {
            return await _service.GetCategories();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<CategoryModel>> Get(int id)
        {
            CategoryModel CategoryModel = await _service.GetCategoryById(id);
            if (CategoryModel == null)
            {
                return NotFound();
            }
            return CategoryModel;
        }

        [HttpPost]
        public async Task<ActionResult<CategoryModel>> Post([FromBody] CategoryModel CategoryModel)
        {
            try
            {
                CategoryModel modelSaved = await _service.Create(CategoryModel);
                return CreatedAtAction("Get", new { id = modelSaved.Id }, modelSaved);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPatch("{id}")]
        public async Task<IActionResult> Patch(int id, [FromBody] CategoryModel CategoryModel)
        {
            try
            {
                await _service.Update(id, CategoryModel);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _service.Delete(id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return NoContent();
        }
    }
}
