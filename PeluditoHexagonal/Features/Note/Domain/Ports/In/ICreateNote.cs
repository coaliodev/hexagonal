﻿using PeluditoHexagonal.Features.Note.Domain.Models;

namespace PeluditoHexagonal.Features.Note.Domain.Ports.In
{
    public interface ICreateNote
    {
        Task<NoteModel> Create(NoteModel note);
    }
}
