﻿namespace PeluditoHexagonal.Features.Note.Domain.Ports.In
{
    public interface INoteServicePort : IGetNoteById, IGetNotes, ICreateNote, IUpdateNote, IDeleteNote, IAddTags, IRemoveTags
    {
    }
}
