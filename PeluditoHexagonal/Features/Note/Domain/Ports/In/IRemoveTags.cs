﻿using PeluditoHexagonal.Features.Note.Domain.Models;
using PeluditoHexagonal.Features.User.Domain.Models;

namespace PeluditoHexagonal.Features.Note.Domain.Ports.In
{
    public interface IRemoveTags
    {
        Task<NoteModel> RemoveTags(int noteId, List<int> tagIdList);
    }
}
