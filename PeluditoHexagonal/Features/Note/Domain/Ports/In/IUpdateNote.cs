﻿using PeluditoHexagonal.Features.Note.Domain.Models;

namespace PeluditoHexagonal.Features.Note.Domain.Ports.In
{
    public interface IUpdateNote
    {
        Task<NoteModel> Update(int id, NoteModel note);
    }
}
