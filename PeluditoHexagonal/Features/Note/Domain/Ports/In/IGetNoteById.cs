﻿using PeluditoHexagonal.Features.Note.Domain.Models;

namespace PeluditoHexagonal.Features.Note.Domain.Ports.In
{
    public interface IGetNoteById
    {
        Task<NoteModel> GetNoteById(int id);
    }
}
