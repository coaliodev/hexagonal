﻿using PeluditoHexagonal.Features.Note.Domain.Models;

namespace PeluditoHexagonal.Features.Note.Domain.Ports.In
{
    public interface IGetNotes
    {
        Task<List<NoteModel>> GetNotes();
    }
}
