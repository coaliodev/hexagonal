﻿using PeluditoHexagonal.Features.Note.Domain.Models;

namespace PeluditoHexagonal.Features.Note.Domain.Ports.In
{
    public interface IAddTags
    {
        Task<NoteModel> AddTags(int noteId, List<int> tagIdList);
    }
}
