﻿using PeluditoHexagonal.Features.Note.Domain.Models;
using PeluditoHexagonal.Shared.Domain.Ports.Out;

namespace PeluditoHexagonal.Features.Note.Domain.Ports.Out
{
    public interface INoteDbAdapterPort : IDbCrudAdapter<NoteModel>
    {
        Task<NoteModel> AddTags(int noteId, List<int> tagIdList);
        Task<NoteModel> RemoveTags(int noteId, List<int> tagIdList);
    }
}
