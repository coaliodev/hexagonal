﻿namespace PeluditoHexagonal.Features.Note.Domain
{
    public class NoteNotFoundException : Exception
    {
        public NoteNotFoundException(int id) : base($"The note with id {id} was not found")
        {
        }
        public NoteNotFoundException(string email) : base($"The note with email {email} was not found")
        {
        }
    }
}
