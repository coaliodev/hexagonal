﻿using PeluditoHexagonal.Features.User.Domain.Models;
using PeluditoHexagonal.Features.User.Domain.Ports.In;
using PeluditoHexagonal.Features.User.Domain.Ports.Out;
using PeluditoHexagonal.Features.User.Domain;
using PeluditoHexagonal.Features.Note.Domain.Ports.In;
using PeluditoHexagonal.Features.Note.Domain.Models;
using PeluditoHexagonal.Features.Note.Domain.Ports.Out;

namespace PeluditoHexagonal.Features.Note.Application.UseCases
{
    public class RemoveTagsImpl : IRemoveTags
    {
        private readonly INoteDbAdapterPort _adapter;
        private readonly IGetNoteById _getUserById;

        public RemoveTagsImpl(INoteDbAdapterPort adapter, IGetNoteById getUserById)
        {
            _adapter = adapter;
            _getUserById = getUserById;
        }

        public async Task<NoteModel> RemoveTags(int userId, List<int> tagIdList)
        {
            var user = await _getUserById.GetNoteById(userId);
            if (user == null)
            {
                throw new UserNotFoundException(userId);
            }

            return await _adapter.RemoveTags(userId, tagIdList);
        }
    }
}
