﻿using PeluditoHexagonal.Features.Note.Domain.Models;
using PeluditoHexagonal.Features.Note.Domain.Ports.In;
using PeluditoHexagonal.Features.Note.Domain.Ports.Out;
using PeluditoHexagonal.Features.Tag.Domain;
using PeluditoHexagonal.Features.Note.Domain;

namespace PeluditoHexagonal.Features.Note.Application.UseCases
{
    public class AddTagsImpl : IAddTags
    {
        private readonly INoteDbAdapterPort _adapter;
        private readonly IGetNoteById _getNoteById;

        public AddTagsImpl(INoteDbAdapterPort adapter, IGetNoteById getNoteById)
        {
            _adapter = adapter;
            _getNoteById = getNoteById;
        }

        public async Task<NoteModel> AddTags(int noteId, List<int> tagIdList)
        {
            var note = await _getNoteById.GetNoteById(noteId);

            if (note == null)
            {
                // We could not find the note, so let's throw here
                throw new NoteNotFoundException(noteId);
            }

            return await _adapter.AddTags(noteId, tagIdList);
        }
    }
}
