﻿using PeluditoHexagonal.Features.Note.Domain.Models;
using PeluditoHexagonal.Features.Note.Domain.Ports.In;
using PeluditoHexagonal.Features.Note.Domain.Ports.Out;

namespace PeluditoHexagonal.Features.Note.Application.UseCases
{
    public class UpdateNoteImpl : IUpdateNote
    {
        private readonly INoteDbAdapterPort _adapter;

        public UpdateNoteImpl(INoteDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<NoteModel> Update(int id, NoteModel note)
        {
            return _adapter.Update(id, note);
        }
    }
}
