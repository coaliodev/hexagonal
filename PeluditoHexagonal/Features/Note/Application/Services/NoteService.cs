﻿using PeluditoHexagonal.Features.Note.Domain.Models;
using PeluditoHexagonal.Features.Note.Domain.Ports.In;
using PeluditoHexagonal.Features.User.Domain.Models;

namespace PeluditoHexagonal.Features.Note.Application.Services
{
    public class NoteService : INoteServicePort
    {
        private readonly IGetNotes _getNotes;
        private readonly IGetNoteById _getNoteById;
        private readonly ICreateNote _createNote;
        private readonly IUpdateNote _updateNote;
        private readonly IDeleteNote _deleteNote;

        private readonly IAddTags _addTags;
        private readonly IRemoveTags _removeTags;

        public NoteService(
            IGetNotes getNotes,
            IGetNoteById getNoteById,
            ICreateNote createNote,
            IUpdateNote updateNote,
            IDeleteNote deleteNote,
            IAddTags addTags,
            IRemoveTags removeTags
        )
        {
            _getNotes = getNotes;
            _getNoteById = getNoteById;
            _createNote = createNote;
            _updateNote = updateNote;
            _deleteNote = deleteNote;

            _addTags = addTags;
            _removeTags = removeTags;
        }

        public Task<List<NoteModel>> GetNotes()
        {
            return _getNotes.GetNotes();
        }

        public Task<NoteModel> GetNoteById(int id)
        {
            return _getNoteById.GetNoteById(id);
        }

        public Task<NoteModel> Create(NoteModel note)
        {
            return _createNote.Create(note);
        }

        public Task<NoteModel> Update(int id, NoteModel note)
        {
            return _updateNote.Update(id, note);
        }

        public Task<int> Delete(int id)
        {
            return _deleteNote.Delete(id);
        }
        public Task<NoteModel> AddTags(int noteId, List<int> tagIdList)
        {
            return _addTags.AddTags(noteId, tagIdList);
        }

        public Task<NoteModel> RemoveTags(int noteId, List<int> tagIdList)
        {
            return _removeTags.RemoveTags(noteId, tagIdList);
        }
    }
}
