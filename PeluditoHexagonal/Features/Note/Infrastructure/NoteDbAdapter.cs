﻿using PeluditoHexagonal.Features.Note.Domain.Models;
using PeluditoHexagonal.Features.Note.Domain.Ports.Out;
using PeluditoHexagonal.Shared.Infrastructure.Context;
using PeluditoHexagonal.Shared.Infrastructure.DapperContext;
using Microsoft.EntityFrameworkCore;
using PeluditoHexagonal.Features.User.Domain.Models;
using PeluditoHexagonal.Features.Tag.Infrastructure;
using System.Collections.Generic;

namespace PeluditoHexagonal.Features.Note.Infrastructure
{
    public class NoteDbAdapter : INoteDbAdapterPort
    {
        private readonly AppDbContext _db;
        private readonly DapperContext _dapperContext;

        public NoteDbAdapter(AppDbContext db, DapperContext dapperContext)
        {
            _db = db;
            _dapperContext = dapperContext;
        }

        public async Task<List<NoteModel>> FindAll()
        {
            List<NoteModel> notes = await _db.Notes
                .Select(x => x.ToModel())
                .ToListAsync();

            return notes;
        }

        public async Task<NoteModel> FindById(int id)
        {
            NoteEntity? noteFound = await _db.Notes.FirstOrDefaultAsync(i => i.Id == id);

            var noteModel = noteFound?.ToModel();

            if (noteModel != null && noteFound != null)
            {
                noteModel.Tags = noteFound.LoadTags();
            }

            return noteFound == null
                ? throw new Exception($"Note with id {id} not found")
                : noteFound.ToModel();
        }

        public async Task<NoteModel> Create(NoteModel model)
        {
            try
            {
                NoteEntity entity = new NoteEntity().FromModel(model);
                _db.Notes.Add(entity);
                await _db.SaveChangesAsync();
                return entity.ToModel();
            }
            catch (Exception e)
            {
                throw new Exception("Error creating note", e);
            }
        }

        public async Task<NoteModel> Update(int id, NoteModel model)
        {
            NoteEntity? entity = await _db.Notes.FirstOrDefaultAsync(x => x.Id == id);

            if (entity == null)
            {
                throw new Exception("Note not found!");
            }

            try
            {
                entity.UpdatePropsFromModel(model);

                _db.Entry(entity).State = EntityState.Modified;
                await _db.SaveChangesAsync();

                return entity.ToModel();
            }
            catch (Exception e)
            {
                throw new Exception("Error updating note", e);
            }
        }

        public async Task<int> Delete(int id)
        {
            NoteEntity? noteEntity = await _db.Notes.FindAsync(id);

            if (noteEntity == null)
            {
                throw new Exception("Note not found!");
            }

            try
            {
                _db.Notes.Remove(noteEntity);

                await _db.SaveChangesAsync();

                return id;
            }
            catch (Exception e)
            {
                throw new Exception("Error deleting note", e);
            }
        }
        public async Task<NoteModel> AddTags(int noteId, List<int> tagIdList)
        {
            NoteEntity? noteEntity = await _db.Notes
                .FirstOrDefaultAsync(x => x.Id == noteId);

            if (noteEntity == null)
            {
                throw new Exception("Note not found!");
            }

            try
            {
                var existingTags = noteEntity.Tags.ToList();
                List<TagEntity> tagsFound = await _db.Tags
                    .Where(x => tagIdList.Contains(x.Id))
                    .ToListAsync();

                if (!tagsFound.Any())
                {
                    throw new Exception("Tags not found");
                }

                existingTags.AddRange(tagsFound);

                noteEntity.Tags = existingTags;

                await _db.SaveChangesAsync();

                var noteModel = noteEntity.ToModel();

                noteModel.Tags = noteEntity.LoadTags();

                return noteModel;
            }
            catch (Exception e)
            {
                throw new Exception("Error adding tags to note", e);
            }
        }

        public async Task<NoteModel> RemoveTags(int noteId, List<int> tagIdList)
        {
            NoteEntity? noteEntity = await _db.Notes
                .FirstOrDefaultAsync(x => x.Id == noteId);

            if (noteEntity == null)
            {
                throw new Exception("Note not found!");
            }

            try
            {
                var existingTags = noteEntity.Tags.ToList();
                List<TagEntity> tagsFound = await _db.Tags
                    .Where(x => tagIdList.Contains(x.Id))
                    .ToListAsync();

                if (!tagsFound.Any())
                {
                    throw new Exception("Tags not found");
                }

                existingTags = existingTags
                    .Where(x => !tagIdList.Contains(x.Id))
                    .ToList();

                noteEntity.Tags = existingTags;

                await _db.SaveChangesAsync();

                var noteModel = noteEntity.ToModel();

                noteModel.Tags = noteEntity.LoadTags();

                return noteModel;
            }
            catch (Exception e)
            {
                throw new Exception("Error removing tags from note", e);
            }
        }

    }
}
