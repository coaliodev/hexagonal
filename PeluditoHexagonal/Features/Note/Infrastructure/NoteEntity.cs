﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PeluditoHexagonal.Features.User.Infrastructure;
using PeluditoHexagonal.Features.Tag.Infrastructure;
using PeluditoHexagonal.Features.Note.Domain.Models;
using System.Data;
using PeluditoHexagonal.Features.Tag.Domain.Models;

namespace PeluditoHexagonal.Features.Note.Infrastructure
{
    [Table("Notes")]
    public class NoteEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public int UserId { get; set; }

        [Required]
        public int CategoryId { get; set; }

        [Required]
        [StringLength(100)]
        public string Title { get; set; }

        [Required]
        public string Content { get; set; }

        [Required]
        [Column(TypeName = "datetime")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreationDate { get; set; }

        [ForeignKey("UserId")]
        public virtual UserEntity User { get; set; }

        [ForeignKey("CategoryId")]
        public virtual CategoryEntity Category { get; set; }

        public virtual ICollection<TagEntity> Tags { get; set; }

        public NoteEntity FromModel(NoteModel model)
        {
            Id = model.Id;
            UserId = model.UserId;
            CategoryId = model.CategoryId;
            Title = model.Title;
            Content = model.Content;

            return this;
        }

        public NoteModel ToModel()
        {
            var model = new NoteModel
            {
                Id = Id,
                UserId = UserId,
                CategoryId = CategoryId,
                Title = Title,
                Content = Content
            };

            return model;
        }

        public void UpdatePropsFromModel(NoteModel model)
        {
            UserId = model.UserId;
            CategoryId = model.CategoryId;
            Title = model.Title;
            Content = model.Content;
        }

        public ICollection<TagModel> LoadTags()
        {
            return Tags?.Select(x => x.ToModel()).ToList() ?? new List<TagModel>();
        }
    }
}
