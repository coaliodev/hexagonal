﻿using PeluditoHexagonal.Features.Note.Domain.Models;
using PeluditoHexagonal.Features.Note.Domain.Ports.In;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace PeluditoHexagonal.Features.Note.Infrastructure
{
    [Route("api/Notes")]
    [ApiController]
    [Authorize]
    public class NoteController : ControllerBase
    {
        private readonly INoteServicePort _service;

        public NoteController(INoteServicePort service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<NoteModel>>> Get()
        {
            return await _service.GetNotes();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<NoteModel>> Get(int id)
        {
            NoteModel noteModel = await _service.GetNoteById(id);
            if (noteModel == null)
            {
                return NotFound();
            }
            return noteModel;
        }

        [HttpPost]
        public async Task<ActionResult<NoteModel>> Post([FromBody] NoteModel noteModel)
        {
            try
            {
                NoteModel modelSaved = await _service.Create(noteModel);
                return CreatedAtAction("Get", new { id = modelSaved.Id }, modelSaved);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPatch("{id}")]
        public async Task<IActionResult> Patch(int id, [FromBody] NoteModel noteModel)
        {
            try
            {
                await _service.Update(id, noteModel);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _service.Delete(id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return NoContent();
        }
    }
}
