﻿using PeluditoHexagonal.Features.Category.Domain.Ports.In;
using PeluditoHexagonal.Features.User.Domain;
using PeluditoHexagonal.Features.User.Domain.Models;
using PeluditoHexagonal.Features.User.Domain.Ports.In;
using Microsoft.AspNetCore.Mvc;

namespace PeluditoHexagonal.Features.User.Infrastructure
{
    [Route("api/Users")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserServicePort _service;

        public UserController(IUserServicePort service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserModel>>> Get()
        {
            return await _service.GetUsers();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserModel>> Get(int id)
        {
            UserModel? userModel = await _service.GetUserById(id);

            if (userModel == null)
            {
                return NotFound($"User with id {id} not found");
            }

            return userModel;
        }

        [HttpPost]
        public async Task<ActionResult<UserModel>> Post([FromBody] UserModel userModel)
        {
            try
            {
                UserModel userModelSaved = await _service.Create(userModel);
                return CreatedAtAction("Get", new { id = userModelSaved.Id }, userModelSaved);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPatch("{id}")]
        public async Task<IActionResult> Patch(int id, UserModel userModel)
        {
            try
            {
                await _service.Update(id, userModel);
            }
            catch (UserNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _service.Delete(id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return NoContent();
        }
    }
}
