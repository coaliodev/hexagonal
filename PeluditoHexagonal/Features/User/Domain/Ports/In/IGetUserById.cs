﻿using PeluditoHexagonal.Features.User.Domain.Models;

namespace PeluditoHexagonal.Features.User.Domain.Ports.In
{
    public interface IGetUserById
    {
        Task<UserModel?> GetUserById(int id);
    }
}
