﻿using PeluditoHexagonal.Features.User.Domain.Models;

namespace PeluditoHexagonal.Features.User.Domain.Ports.In
{
    public interface IGetUsers
    {
        Task<List<UserModel>> GetUsers();
    }
}
