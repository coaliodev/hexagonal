﻿
namespace PeluditoHexagonal.Features.User.Domain.Ports.In
{
    public interface IUserServicePort : IGetUsers, IGetUserById, ICreateUser, IUpdateUser, IDeleteUser
    {
    }
}
