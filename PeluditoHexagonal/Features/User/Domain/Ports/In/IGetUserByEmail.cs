﻿using PeluditoHexagonal.Features.User.Domain.Models;

namespace PeluditoHexagonal.Features.User.Domain.Ports.In
{
    public interface IGetUserByEmail
    {
        Task<UserModel?> GetUserByEmail(string email);
    }
}
