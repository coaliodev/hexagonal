﻿using PeluditoHexagonal.Features.User.Domain.Models;

namespace PeluditoHexagonal.Features.User.Domain.Ports.In
{
    public interface ICreateUser
    {
        Task<UserModel> Create(UserModel user);
    }
}
