﻿using PeluditoHexagonal.Features.User.Domain.Models;

namespace PeluditoHexagonal.Features.User.Domain.Ports.In
{
    public interface IUpdateUser
    {
        Task<UserModel> Update(int id, UserModel user);
    }
}
