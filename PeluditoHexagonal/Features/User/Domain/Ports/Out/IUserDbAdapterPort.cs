﻿using PeluditoHexagonal.Features.User.Domain.Models;
using PeluditoHexagonal.Shared.Domain.Ports.Out;

namespace PeluditoHexagonal.Features.User.Domain.Ports.Out
{
    public interface IUserDbAdapterPort : IDbCrudAdapter<UserModel>
    {
        Task<UserModel?> FindByEmail(string email);
    }
}
