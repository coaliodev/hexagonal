﻿namespace PeluditoHexagonal.Features.User.Domain
{
    public class UserNotFoundException : Exception
    {
        public UserNotFoundException(int id) : base($"The user with id {id} was not found")
        {
        }
        public UserNotFoundException(string email) : base($"The user with email {email} was not found")
        {
        }
    }
}
