﻿using PeluditoHexagonal.Features.Note.Domain.Ports.In;
using PeluditoHexagonal.Features.User.Domain.Models;
using PeluditoHexagonal.Features.User.Domain.Ports.In;

namespace PeluditoHexagonal.Features.User.Application.Services
{
    public class UserService : IUserServicePort
    {
        private readonly IGetUsers _getUsersService;
        private readonly IGetUserById _getUserByIdService;
        private readonly ICreateUser _createUserService;
        private readonly IUpdateUser _updateUserService;
        private readonly IDeleteUser _deleteUserService;

        public UserService(
            IGetUsers getUsersService,
            IGetUserById getUserByIdService,
            ICreateUser createUserService,
            IUpdateUser updateUserService,
            IDeleteUser deleteUserService
            )
        {
            _getUsersService = getUsersService;
            _getUserByIdService = getUserByIdService;
            _createUserService = createUserService;
            _updateUserService = updateUserService;
            _deleteUserService = deleteUserService;
        }

        public Task<List<UserModel>> GetUsers()
        {
            return _getUsersService.GetUsers();
        }

        public Task<UserModel?> GetUserById(int id)
        {
            return _getUserByIdService.GetUserById(id);
        }

        public Task<UserModel> Create(UserModel user)
        {
            return _createUserService.Create(user);
        }

        public Task<UserModel> Update(int id, UserModel user)
        {
            return _updateUserService.Update(id, user);
        }

        public Task<int> Delete(int id)
        {
            return _deleteUserService.Delete(id);
        }
    }
}
