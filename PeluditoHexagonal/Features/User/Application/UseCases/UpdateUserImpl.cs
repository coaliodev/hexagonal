﻿using PeluditoHexagonal.Features.User.Domain;
using PeluditoHexagonal.Features.User.Domain.Models;
using PeluditoHexagonal.Features.User.Domain.Ports.In;
using PeluditoHexagonal.Features.User.Domain.Ports.Out;

namespace PeluditoHexagonal.Features.User.Application.UseCases
{
    public class UpdateUserImpl : IUpdateUser
    {
        private readonly IConfiguration _config;
        private readonly IUserDbAdapterPort _adapter;
        private readonly IGetUserById _getUserById;

        public UpdateUserImpl(IConfiguration config, IUserDbAdapterPort adapter, IGetUserById getUserById)
        {
            _config = config;
            _adapter = adapter;
            _getUserById = getUserById;
        }

        public async Task<UserModel> Update(int id, UserModel user)
        {
            var userFound = await _getUserById.GetUserById(id);
            if (userFound == null)
            {
                throw new UserNotFoundException(id);
            }

            if (user.Password != null)
            {
                var workFactor = _config.GetSection("BCrypt:WorkFactor").Get<int>();
                string passwordHash = BCrypt.Net.BCrypt.EnhancedHashPassword(user.Password, workFactor);
            }

            return await _adapter.Update(id, user);
        }
    }
}
