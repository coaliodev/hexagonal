﻿using PeluditoHexagonal.Features.User.Domain.Models;
using PeluditoHexagonal.Features.User.Domain.Ports.In;
using PeluditoHexagonal.Features.User.Domain.Ports.Out;

namespace PeluditoHexagonal.Features.User.Application.UseCases
{
    public class CreateUserImpl : ICreateUser
    {
        private readonly IConfiguration _config;
        private readonly IUserDbAdapterPort _adapter;

        public CreateUserImpl(IConfiguration config, IUserDbAdapterPort adapter)
        {
            _config = config;
            _adapter = adapter;
        }

        public async Task<UserModel> Create(UserModel user)
        {
            var workFactor = _config.GetSection("BCrypt:WorkFactor").Get<int>();
            string passwordHash = BCrypt.Net.BCrypt.EnhancedHashPassword(user.Password, workFactor);

            return await _adapter.Create(user);
        }
    }
}
