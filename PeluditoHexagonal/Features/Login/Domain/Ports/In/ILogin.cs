﻿using PeluditoHexagonal.Features.Login.Domain.Models;

namespace PeluditoHexagonal.Features.Login.Domain.Ports.In
{
    public interface ILogin
    {
        Task<string> Login(LoginModel login);
    }
}
