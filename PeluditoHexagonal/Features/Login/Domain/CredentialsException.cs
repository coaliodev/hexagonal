﻿namespace PeluditoHexagonal.Features.Login.Domain
{
    public class CredentialsException : Exception
    {
        public CredentialsException() : base("The credentials provided are invalid")
        {
        }
    }
}
