﻿using PeluditoHexagonal.Features.Login.Domain.Models;
using PeluditoHexagonal.Features.Login.Domain.Ports.In;
using PeluditoHexagonal.Features.Login.Domain;
using PeluditoHexagonal.Features.User.Domain;
using Microsoft.AspNetCore.Mvc;

// This controller allows us to handle login requests

namespace PeluditoHexagonal.Features.Login.Infrastructure
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly ILogin _login;

        public LoginController(ILogin login)
        {
            _login = login;
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody] LoginModel login)
        {
            try
            {
                var token = await _login.Login(login);

                return Ok(token);
            }
            catch (UserNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (CredentialsException ex)
            {
                return Unauthorized(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
