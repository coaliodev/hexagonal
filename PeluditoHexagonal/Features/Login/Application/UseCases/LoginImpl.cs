﻿using PeluditoHexagonal.Features.Login.Domain;
using PeluditoHexagonal.Features.Login.Domain.Models;
using PeluditoHexagonal.Features.Login.Domain.Ports.In;
using PeluditoHexagonal.Features.User.Domain;
using PeluditoHexagonal.Features.User.Domain.Ports.In;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Collections.Generic;

namespace PeluditoHexagonal.Features.Login.Application.UseCases
{
    public class LoginImpl : ILogin
    {
        private readonly IConfiguration _config;
        private readonly IGetUserByEmail _getUserByEmail;

        public LoginImpl(IConfiguration config, IGetUserByEmail getUserByEmail)
        {
            _config = config;
            _getUserByEmail = getUserByEmail;
        }

        public async Task<string> Login(LoginModel login)
        {
            var userFound = await _getUserByEmail.GetUserByEmail(login.Email);

            if (userFound == null)
            {
                throw new UserNotFoundException(login.Email);
            }

            var passwordIsValid = BCrypt.Net.BCrypt.EnhancedVerify(login.Password, userFound.Password);

            if (!passwordIsValid)
            {
                throw new CredentialsException();
            }

            var secretKey = _config.GetSection("Jwt:Key").Get<string>();

            if (string.IsNullOrEmpty(secretKey))
            {
                throw new Exception("Token configuration not found");
            }

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var tokenHandler = new JwtSecurityTokenHandler();

            var claims = new List<Claim> {
                new Claim(ClaimTypes.GivenName, userFound.Name),
                new Claim(ClaimTypes.Email, userFound.Email)
            };

            var tokenDesc = new SecurityTokenDescriptor
            {
                Expires = DateTime.UtcNow.AddMinutes(30), // Set token expiration time
            };

            var token = tokenHandler.CreateToken(tokenDesc);

            return tokenHandler.WriteToken(token);
        }
    }
}