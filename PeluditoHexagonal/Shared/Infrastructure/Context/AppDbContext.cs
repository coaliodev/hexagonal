﻿using PeluditoHexagonal.Features.Note.Infrastructure;
using PeluditoHexagonal.Features.Tag.Infrastructure;
using PeluditoHexagonal.Features.User.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace PeluditoHexagonal.Shared.Infrastructure.Context
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        public DbSet<NoteEntity> Notes { get; set; }
        public DbSet<UserEntity> Users { get; set; }
        public DbSet<CategoryEntity> Categories { get; set; }

        public DbSet<TagEntity> Tags { get; set; }
    }
}
