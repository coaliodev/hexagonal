﻿using PeluditoHexagonal.Features.Note.Infrastructure;
using PeluditoHexagonal.Features.Tag.Infrastructure;
using PeluditoHexagonal.Features.User.Infrastructure;

namespace PeluditoHexagonal.Shared.Infrastructure.Context
{
    public static class SeedData
    {
        public static void Initialize(AppDbContext context)
        {
            SeedTags(context);
            SeedUsers(context);
            SeedNotes(context);
        }

        private static void SeedTags(AppDbContext context)
        {
            if (!context.Tags.Any())
            {
                var tags = new List<TagEntity>
                {
                    new TagEntity { Name = "Admin", Description = "Administrator tag" },
                    new TagEntity { Name = "Instructor", Description = "Instructor tag" },
                    new TagEntity { Name = "Student", Description = "Student tag" }
                };

                context.Tags.AddRange(tags);
                context.SaveChanges();
            }
        }

        private static void SeedUsers(AppDbContext context)
        {
            if (!context.Users.Any())
            {
                var users = new List<UserEntity>
                {
                    new UserEntity {
                        Name = "Peludito",
                        LastName = "El patron",
                        Email = "peludito.patron@outlook.com",
                        // Password = "password123",
                        Password = "$2a$15$pfXWBxApfwBI3icmhbfenOFUeKw3YZnz4xJMlbiON/HXViNmbgx7S"
                    },
                    new UserEntity {
                        Name = "Elena",
                        LastName = "García",
                        Email = "elena.garcia@outlook.com",
                        //Password = "password123",
                        Password = "$2a$15$pfXWBxApfwBI3icmhbfenOFUeKw3YZnz4xJMlbiON/HXViNmbgx7S"
                    },
                    new UserEntity {
                        Name = "Gabriel",
                        LastName = "Fernández",
                        Email = "gabriel.fernandez@outlook.com",
                        //Password = "password123",
                        Password = "$2a$15$pfXWBxApfwBI3icmhbfenOFUeKw3YZnz4xJMlbiON/HXViNmbgx7S"
                    },
                    new UserEntity {
                        Name = "Juan",
                        LastName = "Martínez",
                        Email = "juan.martinez@outlook.com",
                        //Password = "password123",
                        Password = "$2a$15$pfXWBxApfwBI3icmhbfenOFUeKw3YZnz4xJMlbiON/HXViNmbgx7S",
                    },
                    new UserEntity {
                        Name = "Isabella",
                        LastName = "López",
                        Email = "isabella.lopez@outlook.com",
                        //Password = "password123",
                        Password = "$2a$15$pfXWBxApfwBI3icmhbfenOFUeKw3YZnz4xJMlbiON/HXViNmbgx7S",
                    },
                    new UserEntity {
                        Name = "Valentina",
                        LastName = "Hernández",
                        Email = "valentina.hernandez@outlook.com",
                        //Password = "password123",
                        Password = "$2a$15$pfXWBxApfwBI3icmhbfenOFUeKw3YZnz4xJMlbiON/HXViNmbgx7S",
                    }
                };

                context.Users.AddRange(users);
                context.SaveChanges();
            }
        }

        private static void SeedNotes(AppDbContext context)
        {
            if (!context.Notes.Any()) {
                var notes = new List<NoteEntity> {
                    new NoteEntity
                    {
                        UserId = 1, 
                        CategoryId = 1,
                        Title = "NestJS Note",
                        Content = "Learn NestJS for building scalable server-side applications.",
                        Tags = new List<TagEntity> {
                            new TagEntity { Name = "NestJS" },
                            new TagEntity { Name = "Backend" }
                        }
                    },
                    new NoteEntity
                    {
                        UserId = 1, 
                        CategoryId = 1,
                        Title = "React Note",
                        Content = "Build modern web applications with React.",
                        Tags = new List<TagEntity> {
                            new TagEntity { Name = "React" },
                            new TagEntity { Name = "Frontend" }
                        }
                    }
                };

                context.Notes.AddRange(notes);
                context.SaveChanges();
            }
        }
    }
}
