﻿using PeluditoHexagonal.Shared.Infrastructure.Context;
using Microsoft.Data.SqlClient;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq.Expressions;
using System.Reflection;

namespace PeluditoHexagonal.Shared.Infrastructure.DapperContext
{
    public class DapperContext
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;

        public IDbConnection CreateConnection() => new SqlConnection(_connectionString);

        public DapperContext(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("DatabaseConnection") ?? string.Empty;
        }

    }
}
